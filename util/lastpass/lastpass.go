package lastpass

import (
	"context"
	"fmt"
	lp "github.com/ansd/lastpass-go"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"os"
	"os/signal"
	"os/user"
	"path/filepath"
	"syscall"
)

type Lastpass interface {
	Accounts(ctx context.Context) ([]*lp.Account, error)
}

var _ Lastpass = &lp.Client{}

func New() (Lastpass, error) {
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	buff, _ := ioutil.ReadFile(filepath.Join(usr.HomeDir, ".lpass", "username"))
	username := string(buff)
	if len(username) == 0 {
		if err := input("Enter Lastpass username: ", &username); err != nil {
			return nil, err
		}
	} else {
		fmt.Printf("Using username %s\n", username)
	}
	pass := ""
	if err := password("Enter Lastpass password: ", &pass); err != nil {
		return nil, err
	}
	otp := ""
	if err := input("Enter OTP (leave blank if 2FA is not used): ", &otp); err != nil {
		return nil, err
	}
	var opts []lp.ClientOption
	if len(otp) > 0 {
		opts = append(opts, lp.WithOneTimePassword(otp))
	}
	client, err := lp.NewClient(context.Background(), username, pass, opts...)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func input(prompt string, v *string) error {
	fmt.Print(prompt)
	_, err := fmt.Scanln(v)
	if err != nil {
		return err
	}
	return nil
}

func password(prompt string, v *string) error {
	initialTermState, err := terminal.GetState(syscall.Stdin)
	if err != nil {
		return err
	}

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)
	go func() {
		<-c
		_ = terminal.Restore(syscall.Stdin, initialTermState)
		os.Exit(1)
	}()
	fmt.Print(prompt)
	p, err := terminal.ReadPassword(syscall.Stdin)
	fmt.Println("")
	if err != nil {
		return err
	}
	*v = string(p)
	signal.Stop(c)
	return nil
}
