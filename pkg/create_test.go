package pkg

import (
	"bytes"
	"context"
	"errors"
	lp "github.com/ansd/lastpass-go"
	"gitlab.com/sparetimecoders/k8s-go/config"
	"gitlab.com/sparetimecoders/k8s-go/util"
	"gitlab.com/sparetimecoders/k8s-go/util/aws"
	"gitlab.com/sparetimecoders/k8s-go/util/kops"
	"gitlab.com/sparetimecoders/k8s-go/util/lastpass"
	"io/ioutil"
	"os"
	"testing"
)

func TestCreate(t *testing.T) {
	configWithDocker, f1 := createTempFile(`
name: gotest
dnsZone: example.com
kubernetesVersion: 1.12.2
masterZones:
  - a
cloudLabels: {}
nodes:
  policies:
    - actions:
        - s3:Get
      resources:
        - arn:aws:s3:::blutti
      effect: Allow
dockerconfig: lp://some/key:notes
`)
	configWithoutDocker, f2 := createTempFile(`
name: gotest
dnsZone: example.com
kubernetesVersion: 1.12.2
masterZones:
  - a
cloudLabels: {}
nodes:
  policies:
    - actions:
        - s3:Get
      resources:
        - arn:aws:s3:::blutti
      effect: Allow
`)
	defer f1()
	defer f2()

	type args struct {
		file string
		f    util.Factory
	}
	tests := []struct {
		name    string
		args    args
		wantOut string
		wantErr bool
	}{
		{
			name: "missing file",
			args: args{
				file: "missing.yaml",
				f:    nil,
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "cluster exists",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{ExistingCluster: true}
					},
				},
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "create cluster error",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{}, errors.New("error")
							},
						}
					},
				},
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "update values error",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{}, nil
							},
						}
					},
					lpFunc: func() (lastpass.Lastpass, error) {
						return nil, errors.New("error")
					},
				},
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "create secrets error",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{}, nil
							},
							createSecretsFunc: func(config config.ClusterConfig) error {
								if config.Dockerconfig != "value" {
									t.Errorf("Create() got %s, want value", config.Dockerconfig)
								}
								return errors.New("error")
							},
						}
					},
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{
							accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
								return []*lp.Account{
									{
										Name:  "key",
										Group: "some",
										Notes: "value",
									},
								}, nil
							},
						}, nil
					},
				},
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "set IAM policies error",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{Kops: &mockKops{
									getConfigFunc: func() (string, error) {
										return "config", nil
									},
									replaceClusterFunc: func(config string) error {
										wantConfig := "config"
										if config != wantConfig {
											t.Errorf("Create() got %s, want %s", config, wantConfig)
										}
										return errors.New("error")
									},
								}}, nil
							},
							createSecretsFunc: func(config config.ClusterConfig) error {
								return nil
							},
						}
					},
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{
							accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
								return []*lp.Account{
									{
										Name:  "key",
										Group: "some",
										Notes: "value",
									},
								}, nil
							},
						}, nil
					},
				},
			},
			wantOut: "",
			wantErr: true,
		},
		{
			name: "validation error",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{Kops: &mockKops{
									getConfigFunc: func() (string, error) {
										return "config", nil
									},
									replaceClusterFunc: func(config string) error {
										return nil
									},
									getInstanceGroupFunc: func(name string) ([]byte, error) {
										switch name {
										case "nodes", "master-eu-west-1a":
										default:
											t.Errorf("Create() got %s, want one of nodes, ...", name)
										}
										return []byte(`kind: InstanceGroup`), nil
									},
									replaceInstanceGroupFunc: func(name string, data []byte) error {
										return nil
									},
									updateClusterFunc: func() error {
										return nil
									},
									validateClusterFunc: func() (string, bool) {
										return "validation error", true
									},
								}}, nil
							},
							createSecretsFunc: func(config config.ClusterConfig) error {
								return nil
							},
						}
					},
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{
							accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
								return []*lp.Account{
									{
										Name:  "key",
										Group: "some",
										Notes: "value",
									},
								}, nil
							},
						}, nil
					},
				},
			},
			wantOut: "",
			wantErr: false,
		},
		{
			name: "success",
			args: args{
				file: configWithDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{Kops: &mockKops{
									getConfigFunc: func() (string, error) {
										return "config", nil
									},
									replaceClusterFunc: func(config string) error {
										return nil
									},
									getInstanceGroupFunc: func(name string) ([]byte, error) {
										switch name {
										case "nodes", "master-eu-west-1a":
										default:
											t.Errorf("Create() got %s, want one of nodes, ...", name)
										}
										return []byte(`kind: InstanceGroup`), nil
									},
									replaceInstanceGroupFunc: func(name string, data []byte) error {
										return nil
									},
									updateClusterFunc: func() error {
										return nil
									},
									validateClusterFunc: func() (string, bool) {
										return "", true
									},
								}}, nil
							},
							createSecretsFunc: func(config config.ClusterConfig) error {
								return nil
							},
						}
					},
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{
							accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
								return []*lp.Account{
									{
										Name:  "key",
										Group: "some",
										Notes: "value",
									},
								}, nil
							},
						}, nil
					},
				},
			},
			wantOut: "",
			wantErr: false,
		},
		{
			name: "success without dockerconfig",
			args: args{
				file: configWithoutDocker.Name(),
				f: &mockFactory{
					awsFunc: func() aws.Service {
						return aws.MockService{}
					},
					kopsFunc: func(name, stateStore string) kops.Kops {
						return &mockKops{
							createClusterFunc: func(config config.ClusterConfig) (kops.Cluster, error) {
								return kops.Cluster{Kops: &mockKops{
									getConfigFunc: func() (string, error) {
										return "config", nil
									},
									replaceClusterFunc: func(config string) error {
										return nil
									},
									getInstanceGroupFunc: func(name string) ([]byte, error) {
										switch name {
										case "nodes", "master-eu-west-1a":
										default:
											t.Errorf("Create() got %s, want one of nodes, ...", name)
										}
										return []byte(`kind: InstanceGroup`), nil
									},
									replaceInstanceGroupFunc: func(name string, data []byte) error {
										return nil
									},
									updateClusterFunc: func() error {
										return nil
									},
									validateClusterFunc: func() (string, bool) {
										return "", true
									},
								}}, nil
							},
						}
					},
				},
			},
			wantOut: "",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}
			err := Create(tt.args.file, tt.args.f, out)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotOut := out.String(); gotOut != tt.wantOut {
				t.Errorf("Create() gotOut = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}

func Test_updateSecretValues(t *testing.T) {
	type args struct {
		config       *config.ClusterConfig
		factory      util.Factory
		dockerconfig string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		want    string
	}{
		{
			name:    "empty",
			args:    args{dockerconfig: ""},
			wantErr: false,
			want:    "",
		},
		{
			name: "factory error",
			args: args{
				dockerconfig: "lp://envconfig\\prod/dockercfg:notes",
				factory: &mockFactory{lpFunc: func() (lastpass.Lastpass, error) {
					return nil, errors.New("error")
				}},
			},
			wantErr: true,
			want:    "lp://envconfig\\prod/dockercfg:notes",
		},
		{
			name: "lastpass error",
			args: args{
				dockerconfig: "lp://envconfig\\prod/dockercfg:notes",
				factory: &mockFactory{
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
							return nil, errors.New("error")
						}}, nil
					},
				},
			},
			wantErr: true,
			want:    "lp://envconfig\\prod/dockercfg:notes",
		},
		{
			name: "account not found",
			args: args{
				dockerconfig: "lp://envconfig\\prod/dockercfg:notes",
				factory: &mockFactory{
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
							return []*lp.Account{}, nil
						}}, nil
					},
				},
			},
			wantErr: true,
			want:    "lp://envconfig\\prod/dockercfg:notes",
		},
		{
			name: "success",
			args: args{
				dockerconfig: "lp://envconfig\\prod/dockercfg:notes",
				factory: &mockFactory{
					lpFunc: func() (lastpass.Lastpass, error) {
						return &mockLpClient{accountsFunc: func(ctx context.Context) ([]*lp.Account, error) {
							return []*lp.Account{
								{
									Group: "envconfig\\prod",
									Name:  "other",
									Notes: "other",
								},
								{
									Group: "envconfig\\prod",
									Name:  "dockercfg",
									Notes: "{}",
								},
							}, nil
						}}, nil
					},
				},
			},
			wantErr: false,
			want:    "{}",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cfg := &config.ClusterConfig{Dockerconfig: tt.args.dockerconfig}
			if err := updateSecretValues(cfg, tt.args.factory); (err != nil) != tt.wantErr {
				t.Errorf("updateSecretValues() error = %v, wantErr %v", err, tt.wantErr)
			}
			if cfg.Dockerconfig != tt.want {
				t.Errorf("UpdateSecretValues() got %s, want %s", cfg.Dockerconfig, tt.want)
			}
		})
	}
}

type mockFactory struct {
	awsFunc  func() aws.Service
	kopsFunc func(name, stateStore string) kops.Kops
	lpFunc   func() (lastpass.Lastpass, error)
}

func (m mockFactory) Aws() aws.Service {
	return m.awsFunc()
}

func (m mockFactory) Kops(name, stateStore string) kops.Kops {
	return m.kopsFunc(name, stateStore)
}

func (m mockFactory) Lastpass() (lastpass.Lastpass, error) {
	return m.lpFunc()
}

var _ util.Factory = &mockFactory{}

type mockLpClient struct {
	accountsFunc func(ctx context.Context) ([]*lp.Account, error)
}

func (m mockLpClient) Accounts(ctx context.Context) ([]*lp.Account, error) {
	return m.accountsFunc(ctx)
}

var _ lastpass.Lastpass = &mockLpClient{}

type mockKops struct {
	createClusterFunc        func(config config.ClusterConfig) (kops.Cluster, error)
	updateClusterFunc        func() error
	validateClusterFunc      func() (string, bool)
	createSecretsFunc        func(config config.ClusterConfig) error
	getConfigFunc            func() (string, error)
	replaceClusterFunc       func(config string) error
	getInstanceGroupFunc     func(name string) ([]byte, error)
	replaceInstanceGroupFunc func(name string, data []byte) error
}

func (m mockKops) CreateCluster(config config.ClusterConfig) (kops.Cluster, error) {
	return m.createClusterFunc(config)
}

func (m mockKops) DeleteCluster(config config.ClusterConfig) error {
	panic("implement me")
}

func (m mockKops) UpdateCluster() error {
	return m.updateClusterFunc()
}

func (m mockKops) ReplaceCluster(config string) error {
	return m.replaceClusterFunc(config)
}

func (m mockKops) ValidateCluster() (string, bool) {
	return m.validateClusterFunc()
}

func (m mockKops) GetConfig() (string, error) {
	return m.getConfigFunc()
}

func (m mockKops) ReplaceInstanceGroup(name string, data []byte) error {
	return m.replaceInstanceGroupFunc(name, data)
}

func (m mockKops) GetInstanceGroup(name string) ([]byte, error) {
	return m.getInstanceGroupFunc(name)
}

func (m mockKops) Version() (string, error) {
	panic("implement me")
}

func (m mockKops) MinimumKopsVersionInstalled(requiredKopsVersion string) bool {
	panic("implement me")
}

func (m mockKops) CreateSecrets(config config.ClusterConfig) error {
	return m.createSecretsFunc(config)
}

var _ kops.Kops = &mockKops{}

func createTempFile(content string) (*os.File, func()) {
	tempFile, _ := ioutil.TempFile(os.TempDir(), "abc")
	_ = ioutil.WriteFile(tempFile.Name(), []byte(content), os.ModeExclusive)
	return tempFile, func() { _ = os.RemoveAll(tempFile.Name()) }
}
