module gitlab.com/sparetimecoders/k8s-go

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/Masterminds/semver v1.4.2
	github.com/ansd/lastpass-go v0.1.1
	github.com/aws/aws-sdk-go v1.19.33
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/spf13/cobra v0.0.4
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.4

)

go 1.13
