// +build !prod

package util

import (
	"gitlab.com/sparetimecoders/k8s-go/util/aws"
	"gitlab.com/sparetimecoders/k8s-go/util/kops"
	"gitlab.com/sparetimecoders/k8s-go/util/lastpass"
)

type MockFactory struct {
	ClusterExists bool
	Handler       kops.MockHandler
}

func NewMockFactory() *MockFactory {
	factory := &MockFactory{}
	factory.Handler = kops.MockHandler{
		Cmds:      make(chan string, 100),
		Responses: make(chan string, 100),
	}
	return factory
}

func (c *MockFactory) Aws() aws.Service {
	return aws.MockService{
		ExistingCluster: c.ClusterExists,
	}
}

func (c *MockFactory) Kops(clusterName string, stateStore string) kops.Kops {
	return kops.NewMock(clusterName, c.Handler)
}

func (c *MockFactory) Lastpass() (lastpass.Lastpass, error) {
	return nil, nil
}

var _ Factory = &MockFactory{}
