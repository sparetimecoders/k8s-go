package main

import (
	"gitlab.com/sparetimecoders/k8s-go/cmd"
)

func main() {
	cmd.Execute()
}
