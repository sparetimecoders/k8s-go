package config

import (
	"fmt"
	"github.com/GeertJohan/go.rice"
	"strings"
)

type IngressWs struct {
	Aws *Aws `yaml:"aws" optional:"true"`
}

func (i IngressWs) Manifests(clusterConfig ClusterConfig) (string, error) {
	box := rice.MustFindBox("manifests/ingress_ws")
	manifest := box.MustString("ingress.yaml")

	var replacementString []string
	if i.Aws != nil {
		if i.Aws.CertificateARN != "" {
			replacementString = append(replacementString, fmt.Sprintf("    service.beta.kubernetes.io/aws-load-balancer-ssl-cert: %v", i.Aws.CertificateARN))
		}
		replacementString = append(replacementString, fmt.Sprintf("    service.beta.kubernetes.io/aws-load-balancer-connection-idle-timeout: \"%d\"", i.Aws.Timeout))
		replacementString = append(replacementString, fmt.Sprintf("    service.beta.kubernetes.io/aws-load-balancer-ssl-negotiation-policy: %v", i.Aws.SecurityPolicy))
		replacementString = append(replacementString, fmt.Sprintf("    service.beta.kubernetes.io/aws-load-balancer-ssl-ports: %v", i.Aws.SSLPort))
		replacementString = append(replacementString, fmt.Sprintf("    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: %v", i.Aws.Protocol))
	}
	manifest = strings.Replace(manifest, `    annotations_placeholder: ""`, strings.Join(replacementString, "\n"), 1)
	return strings.Join([]string{manifest, box.MustString("nginx-config.yaml")}, "\n---\n"), nil
}

func (i IngressWs) Name() string {
	return "IngressWs"
}

func (i IngressWs) Policies() Policies {
	return Policies{}
}
